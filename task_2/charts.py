import streamlit as st
import pandas as pd
import numpy as np
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import matplotlib.pyplot as plt

def draw_pie(df,column,title):
    fig = px.pie(df, values='paid_amount', names=column, title=title)
    st.plotly_chart(fig)