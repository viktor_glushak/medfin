import streamlit as st
import pandas as pd
import tools
import numpy as np
from pandas.tseries.offsets import MonthEnd
import style_dashboard
import charts

pd.set_option('display.max_columns', None)

def main():



    st.sidebar.title("Choose action:")
    app_mode = st.sidebar.selectbox("",
        ["Analysis with filters", "Show all data"])
    style_dashboard.main()

    if app_mode == "Analysis with filters":
        try:
            date_input = st.date_input("Choose dates interval (2) below", [], min_value=min_date, max_value=max_date)
            service_category = st.multiselect('Select service category', sorted(df['service_category'].unique()))
            claim_specialty = st.multiselect('Select claim specialty', df['claim_specialty'].unique())
            payer = st.multiselect('Select payer', sorted(df['payer'].unique()))
            show_data_with_filters(date_input, service_category, claim_specialty, payer)

        except:
            st.write()
    elif app_mode == "Show all data":

        st.warning('Long time load')
        show_all_data_witout_filters = st.checkbox('Show all data ?')
        if show_all_data_witout_filters:
            try:
                show_all_data()
            except:
                st.write()


#import data
data = 'claims_test.csv'
df = tools.import_data(data)

#set lowercase for names columns
df.columns = map(str.lower, df.columns)

#convert int to datetype
df['month'] = tools.convert_datetype(df['month'])

#normalize text values in columns
df['claim_specialty'] = df['claim_specialty'].str.upper()

min_date = df['month'].min()

#add all days to end of the month in dataframe for right work calendar
max_date = df['month'].max() + MonthEnd(1)

#@st.cache(suppress_st_warning=True)
def show_data_with_filters(date_input = None, service_category = None, claim_specialty = None, payer = None):

    if not date_input and not service_category and not claim_specialty and not payer:
        return

    range_date_filter = ((df['month'] >= np.datetime64(date_input[0])) & (df['month'] <= np.datetime64(date_input[1]))) if date_input else (
                (df['month'] >= df['month'].min()) & (df['month'] <= df['month'].max()))

    service_category_filter = df['service_category'].isin(service_category) if len(service_category) else df[
        'service_category']

    claim_specialty_filter = df['claim_specialty'].isin(claim_specialty) if len(claim_specialty) else df[
        'claim_specialty']

    payer_filter = df['payer'].isin(payer) if len(payer) else df[
        'payer']

    all_filters = range_date_filter & service_category_filter & claim_specialty_filter & payer_filter

    st.dataframe(df[all_filters])

    st.bar_chart(df[all_filters])

    show_outliers = st.checkbox('Show outliers')
    if show_outliers:
        st.dataframe(
            tools.get_outliers(df[all_filters], ['paid_amount']).sort_values(by='paid_amount', ascending=False))

    max_paid_amount_filter = df[all_filters]['paid_amount'].max()

    if int(max_paid_amount_filter) == 0:
        st.warning("Paid amount is equal zero")
    else:
        paid_amount = st.slider('Paid amount', 0, int(max_paid_amount_filter), (0, int(max_paid_amount_filter * 0.25)),
                                step=1000)
        paid_amount_filter = (df['paid_amount'] >= paid_amount[0]) & (df['paid_amount'] <= paid_amount[1])

        st.dataframe(df[all_filters & paid_amount_filter].sort_values(by='paid_amount', ascending=False))

        charts.draw_pie(df[all_filters], 'month', 'Paid amount of month')
        charts.draw_pie(df[all_filters], 'service_category', 'Paid amount of service category')
        charts.draw_pie(df[all_filters], 'claim_specialty', 'Paid amount of claim specialty')
        charts.draw_pie(df[all_filters], 'payer', 'Paid amount of payer')




#@st.cache(suppress_st_warning=True)
def show_all_data():
    st.dataframe(df)
    st.bar_chart(df)

    show_outliers = st.checkbox('Show outliers')
    if show_outliers:
        st.dataframe(
            tools.get_outliers(df, ['paid_amount']).sort_values(by='paid_amount', ascending=False))

    max_paid_amount_filter = df['paid_amount'].max()
    paid_amount = st.slider('Paid amount', 0, int(max_paid_amount_filter), (0, int(max_paid_amount_filter * 0.25)),
                            step=10000)
    paid_amount_filter = (df['paid_amount'] >= paid_amount[0]) & (df['paid_amount'] <= paid_amount[1])

    st.dataframe(df[paid_amount_filter]).sort_values(ascending=False)

main()