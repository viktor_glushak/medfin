import pandas as pd

def import_data(dataset):
    df = pd.read_csv(dataset)
    return df

#convert zero month (00) to (01) for datetype
def convert_zero_month(x):
    if x[4:6] == '00':
        return x[:3] + '01'
    return x

def convert_datetype(month):
    month = month.apply(str).apply(convert_zero_month)
    return pd.to_datetime(month, format='%Y%m')

def get_bigger_whisker(df, column):
    #copy_df = df.copy()
    df[column] = df[column].replace('[\$,]', '', regex=True).astype(float)
    q3 = df[column].quantile(.75)
    q1 = df[column].quantile(.25)
    iqr = q3 - q1
    result = q3 + (1.5 * iqr)
    return result

def get_outliers(df, arr_columns):
    copy_df = df.copy()
    for column in arr_columns:
        bigger_whisker =  get_bigger_whisker(copy_df, column)
        copy_df =  copy_df[copy_df[column] > bigger_whisker]
    return copy_df