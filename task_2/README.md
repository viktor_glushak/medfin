# Task 2

- Create a web dashboard prototype in Python that allows users to:
    - Create slicers and dicers
    - Filters by date range / ...
    - Show data both in table and plotted formats
- Use paid_amount column for analysis and other columns for filters
- Don’t worry about UI and design, just provide very simple functionality
- You are free to use Flask / FastAPI / plotly / bokeh / etc.
- Normalize text values in columns. Please, provide your approach to do
it in the automatic way as Jupyter notebook. Think about it as you need
to update this dashboard monthly and do not have time to normalize values manually

Please provide this task as a python project.
You can send it as a zip archive / Git repository / Docker

#### Columns:

- month 
- service_category 
- claim_specialty
- payer
- paid_amount 

### How run server (tested on linux)

* (If dont have virtual environment) on CentOS 7 (`sudo yum install python3-virtualenv -y`) 
on Ubuntu (`sudo apt install python3-virtualenv -y`)
* In root of project set virtualenv `sudo python3 -m virtualenv venv`
* Activate venv `source venv/bin/activate`
* Install dependencies `sudo cat requirements.txt | xargs -n 1 venv/bin/pip3 install`
* `sudo venv/bin/streamlit run dashboard.py` it runs on 8501 port by default